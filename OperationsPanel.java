import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class OperationsPanel extends JPanel implements ActionListener{
	
	private JTextField txtText;
	
	private char op;
	private double valor1;
	private double valor2;
	
	private JButton botSoma;
	private JButton botSub;
	private JButton botMult;
	private JButton botDiv;
	private JButton botIgual;
	private JButton botClear;
	
	
	public OperationsPanel(JTextField txtText ) {
		this.txtText = txtText;
		
		setLayout(new GridLayout(5,2));
		
		botSoma = new JButton("+");
		botSoma.addActionListener(this);
		add(botSoma);
		
		botSub = new JButton("-");
		botSub.addActionListener(this);
		add(botSub);
		
		botMult = new JButton("*");
		botMult.addActionListener(this);
		add(botMult);
		
		botDiv = new JButton("/");
		botDiv.addActionListener(this);
		add(botDiv);
		
		botIgual = new JButton("=");
		botIgual.addActionListener(this);
		add(botIgual);
		
		botClear = new JButton("C");
		botClear.addActionListener(this);
		add(botClear);
	}

	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(txtText.getText().isEmpty()) {//verifica se a string esta vazia
			return;
		}
		
		JButton bot = (JButton) e.getSource();//qual botao foi clicado
		
		
		if(bot == botClear) {
			op = '\u0000';//define um null para o ojeto
			txtText.setText("");//vai limpar
		}else if(bot == botIgual) {
			valor2 = Double.parseDouble(txtText.getText());
			double result = 0.0;
			
			if(op == '+') {
				result = valor1 + valor2;
			}else if (op == '-') {
				result = valor1 - valor2;
			}else if (op == '*') {
				result = valor1 * valor2;
			}else if (op == '/') {
				result = valor1 / valor2;
			}
			txtText.setText(String.valueOf(result));//armazenar 
			op = '\u0000';
			valor1= result;
			valor2= 0;
			
			}else {
			op =bot.getText().charAt(0);
			valor1=Double.parseDouble(txtText.getText());
			txtText.setText(" ");
		}
		
	}
}


