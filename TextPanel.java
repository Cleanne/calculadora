import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;

public class TextPanel  extends JPanel{
	
	private JTextField txtNumber;//caixa de texto
	
	
	public TextPanel() {
		setLayout(new FlowLayout());//inseridos lado a lado
		txtNumber = new JTextField(20);//caixa de texto tamanho 20
		txtNumber.setHorizontalAlignment(JTextField.RIGHT);//alinhar os numeros a direita
		txtNumber.setEnabled(false);//desabilitar a caixa de texto, s� usar os bot�es
		add(txtNumber);//adicionar no painel
	}


	public JTextField getTxtNumber() {//para possa passar esses parametros em outros paineis
		return txtNumber;
	}
	
	
	
}