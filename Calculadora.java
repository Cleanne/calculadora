
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Spring;


public class Calculadora extends JFrame {
	
	public Calculadora() {
	super("Calculadora");// titulo da janela
	
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setSize(300,300);//tamanho da janela
	
	
	setLayout(new BorderLayout());//adicionar painel
	
	TextPanel textPanel = new TextPanel();//criar uma instancia
	add(BorderLayout.NORTH, textPanel);//adicionar o painel
	
	JPanel digitoPanel = new JPanel();//painel inferior
	digitoPanel.setLayout(new BorderLayout());
	digitoPanel.add(BorderLayout.CENTER, new NumberPanel(textPanel.getTxtNumber()));//painel da esquerda
	digitoPanel.add(BorderLayout.EAST,new OperationsPanel(textPanel.getTxtNumber()));
	add(BorderLayout.CENTER, digitoPanel);
	
	setVisible(true);//faz com que a janela apare�a
	
	
	}
	public static void main(String[] args) throws Exception{
		new Calculadora();
	}
	

}
